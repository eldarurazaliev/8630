<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


class LanguageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	const RU = 'ru'; // russian code
	const EN = 'en'; // the english one
	const FI = 'fi'; // finnish
	const DE = 'de'; // german
	const FR = 'fr'; // french
	const SE = 'se'; // swedish
	const ES = 'es'; // spannish
	const RUT = 'rut'; // russian translate
	const LANGS = [self::RU, self::EN, self::FI, self::DE, self::FR, self::SE, self::ES, self::RUT];

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'index','view', 'create','update', 'simpleSearch', 'export'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Language;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Language']))
		{
			$model->attributes=$_POST['Language'];
			if($model->save()) {
				$this->actionExport();
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Language']))
		{
			$model->attributes=$_POST['Language'];
			if($model->save()) {
				$this->actionExport();
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->actionExport();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Language',
		    array('pagination' => array(
		          'pageSize' => 30,
			 ),
		      )
		);
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($lang = 'ru')
	{
		
		
		$model=new Language('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Language'])) {
			$model->attributes=$_GET['Language'];
		}
		$this->render('admin',array(
			'model'=>$model,
			'lang'=>$lang,

		));
	}
	
	public function actionExport($lang = 'ru')
	{

	    $outputDir = dirname(dirname(__FILE__)) . '/export/';
	    $outputCaption = '<?php // FILE: language constants, generated at ' . date("m.d.Y G:i:s") . PHP_EOL . 'return array ('. PHP_EOL;
	    $outputBottom = ');' . PHP_EOL;

	    
	    // получаем данные
	    $models = Language::model()->findAll();
	    foreach ($models as $key => $model) {
		foreach (self::LANGS as $lang) {
	            $line = '  ' . var_export($model->name, TRUE) ." => " . var_export($model->$lang, TRUE) . "," . PHP_EOL;
	            $outputArray[$lang][] = $line;
		}
		unset($models[$key]);
	    }

	    // пишем строчки в файлики по очереди
	    foreach ($outputArray as $key => $lines) {
	        $fileName[$key] = $outputDir . "uiconst.class.$key.php";
	        if (file_exists($fileName[$key])) {
	            unlink($fileName[$key]);
	        }
	        if (!$fileHandle[$key] = fopen($fileName[$key], 'a')) {
	            throw new Exception("Could not write to file $fileName[$key]");
	        }
	        fwrite($fileHandle[$key], $outputCaption);
	        unset($fileName[$key]);
    
		foreach ($lines as $id => $line) {
	             fwrite($fileHandle[$key], $line);
	             unset($lines[$id]);
	        }
	        fwrite($fileHandle[$key], $outputBottom);
	        fclose($fileHandle[$key]);
	    }

	    $this->render('export',array(
			'result'=>'done'
	    ));

	}
	
	
	public function actionSimpleSearch($lang = 'ru')
	{
		$model=new Language('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['string'])) {

		    $_GET['Language']['ru'] = $_GET['string'];
		    $_GET['Language']['fi'] = $_GET['string'];
		    $_GET['Language']['en'] = $_GET['string'];
		    $_GET['Language']['de'] = $_GET['string'];
		    $_GET['Language']['fr'] = $_GET['string'];
		    $_GET['Language']['se'] = $_GET['string'];
		    $_GET['Language']['es'] = $_GET['string'];
		    $_GET['Language']['rut'] = $_GET['string'];
		}
		else {$_GET['string'] = null;}

		if(isset($_GET['Language'])) {
			$model->attributes=$_GET['Language'];
		}
		
		$this->render('admin',array(
			'model'=>$model,
			'lang' =>$lang,
			'string' =>$_GET['string']
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Language the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Language::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Language $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='language-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
