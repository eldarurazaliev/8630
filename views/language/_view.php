<?php
/* @var $this LanguageController */
/* @var $data Language */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ru')); ?>:</b>
	<?php echo CHtml::encode($data->ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('en')); ?>:</b>
	<?php echo CHtml::encode($data->en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fi')); ?>:</b>
	<?php echo CHtml::encode($data->fi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('de')); ?>:</b>
	<?php echo CHtml::encode($data->de); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fr')); ?>:</b>
	<?php echo CHtml::encode($data->fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('se')); ?>:</b>
	<?php echo CHtml::encode($data->se); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('es')); ?>:</b>
	<?php echo CHtml::encode($data->es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rut')); ?>:</b>
	<?php echo CHtml::encode($data->rut); ?>
	<br />
	<?php /*

	*/ ?>

</div>