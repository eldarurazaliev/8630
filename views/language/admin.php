<?php
/* @var $this LanguageController */
/* @var $model Language */

$this->breadcrumbs=array(
	'Languages'=>array('index'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List Language', 'url'=>array('index')),
	array('label'=>'Create Language', 'url'=>array('create')),
	array('label'=>'Export Languages', 'url'=>array('export')),

);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#language-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Languages</h1>

<?php
echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png','this is an alternative text',array('width'=>186,'height'=>126));
?>

<div id="language-grid" class="grid-view">
<div class="summary">Select Language</div>
<table class="items">
<thead>
<tr>
<th id="language-grid_c10">NAME</th>

<th id="language-grid_c0">RU</th>
<th id="language-grid_c1">EN</th>
<th id="language-grid_c2">FI</th>
<th id="language-grid_c3">FE</th>
<th id="language-grid_c4">FR</th>
<th id="language-grid_c5">SE</th>
<th id="language-grid_c6">ES</th>
<th id="language-grid_c7">RUT</th>
<th id="language-grid_c7">DO</th>

</tr>
</thead>
<tbody>
<tr class="odd">

<div class="search-form">
<?php 
	isset($_GET['string']) ? $string = $_GET['string'] : $string = null;
	$this->renderPartial('_search_simple',array(
	'model'=>$model,
	'lang'=>$lang,
	'string'=>$string,
)); ?>
</div>


</tbody>
</table>
<div class="keys" style="display:none" title="/index.php?r=language/admin&amp;lang=<?php echo $lang ?>"><span>1</span></div>
</div>

<div class="search-form">
<?php 
	$this->renderPartial('_export',array(
)); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'language-grid',
	'dataProvider'=>$model->search(),
	'ajaxUpdate'=>false,
//	'filter'=>$model,
	'columns'=>array(
//		'id',
		'name',
		$lang,
/*
		'ru',
		'en',
		'fi',
		'de',
		'fr',
		'se',
		'es',
		'rut',

		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
