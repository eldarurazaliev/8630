<?php
/* @var $this LanguageController */
/* @var $model Language */

$this->breadcrumbs=array(
	'Languages'=>array('index'),
);

$this->menu=array(
	array('label'=>'Create Language', 'url'=>array('create')),
	array('label'=>'Manage Language', 'url'=>array('admin')),
);
?>

<h1>Export Language: <?php echo $result; ?></h1>

