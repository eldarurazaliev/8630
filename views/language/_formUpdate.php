<?php
/* @var $this LanguageController */
/* @var $model Language */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'language-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ru'); ?>
		<?php echo $form->textArea($model,'ru',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ru'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'en'); ?>
		<?php echo $form->textArea($model,'en',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'en'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fi'); ?>
		<?php echo $form->textArea($model,'fi',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'fi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'de'); ?>
		<?php echo $form->textArea($model,'de',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'de'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fr'); ?>
		<?php echo $form->textArea($model,'fr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'fr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'se'); ?>
		<?php echo $form->textArea($model,'se',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'se'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'es'); ?>
		<?php echo $form->textArea($model,'es',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'es'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rut'); ?>
		<?php echo $form->textArea($model,'rut',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'rut'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->